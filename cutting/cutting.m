function [Ir, notProcessed] = cutting(I, varargin)
    %CUTTING Summary of this function goes here
    %   Detailed explanation goes here
    % ������ � �����������
    p = inputParser;
    addRequired(p, 'I');
    addOptional(p, 'Debug', 0);
    addOptional(p, 'OperationalSize', 500)
    parse(p, I, varargin{:})
    debug = p.Results.Debug;
    targetSize = p.Results.OperationalSize;
    [w, h, ~] = size(I);
    % ������ ����������� �/�
    bw = rgb2gray(I);
    %��������� ������ ����������� ��� ����� ������� ��������. � ������
    %��������� �������� �������������� ���������� �� ����� ������� ����.
    maxDim = max(w, h);
    if targetSize == -1
        targetSize = maxDim;
        k = 1;
        resized = bw;
    else
        k = targetSize / maxDim;
        resized = imresize(bw, k);
        [w, h, ~] = size(resized);        
    end
    %������� ����� � ������� ����������. ������� ���� ����� ���� ��
    %�����������, �.�. ������ ������ �� ������� ����
    resized = imclose(resized, strel('disk', round(targetSize / 100)));
    %���� ������ c ������, �� ������ ���� �������� �� ������
    %(����� ��� ����� �������� ���� ������)
    gradients = imgradient(resized);
    markers = zeros(w, h);
    markers(1, :) = 1;
    markers(w, :) = 1;
    markers(:, 1) = 1;
    markers(:, h) = 1;
    markers (round(w/2), round(h/2)) = 1;
    prepared = imimposemin(gradients, markers);
    segments = watershed(prepared);
    if debug
        figure; imshow(labeloverlay(resized, segments));
    end
    %�������� ������ ������ ��� ������ ��� ����
    cropped = resized;
    cropped(segments ~= 2) = 0;
    if debug
        figure; imshow(cropped);
    end
    %���� ����� � ����� �� ���
    edges = edge(cropped);
    %���� �����, ���������� ������ �������������
    th_values = -90:0.5:89;
    ht = hough(edges, 'Theta', th_values);
    [hw, hh] = size(ht);
    %peaks = houghpeaks(ht, 4);
    peaks = houghpeaks(ht, 4, 'Threshold', min(w, h) / 8);
    if (size(peaks, 1) < 4)
        disp("Coudn't detect paper edges on image")
        Ir = I;
        notProcessed = 1;
        return
    end
    %������������ ����� �����
    lines = zeros(length(peaks), 4);
    lines(:, 1) = 1; %x1
    lines(:, 2) = h; %x2
    for i=1:length(peaks)
        r = peaks(i, 1) - hw / 2 - 1;
        th = th_values(peaks(i, 2)); % �������� rho � theta
        %if a vertical line, then draw a vertical line centered at x = r
        if (th == 0)
            lines(i, :) = [r, r, 1, w];
        else
            %Compute starting y coordinate (y1)
            lines(i, 3) = (-cosd(th)/sind(th))*lines(i, 1) + (r / sind(th));
            %Compute ending y coordinate (y2)
            lines(i, 4) = (-cosd(th)/sind(th))*lines(i, 2) + (r / sind(th));
        end
    end
    vlind = abs(lines(:, 1) - lines(:, 2)) < abs(lines(:, 3) - lines(:, 4));
    verticalLines = lines(vlind, :);
    horizontalLines = lines(~vlind, :);
    if debug
        lineWidth = 5;
        figure; subplot(1, 2, 1); imshow(I)
        for i = 1:size(verticalLines, 1)
            %Draw the line
            line([verticalLines(i, 1), verticalLines(i, 2)] / k, [verticalLines(i, 3), verticalLines(i, 4)] / k, 'Color', 'Blue', 'LineWidth', lineWidth)
        end
        for i = 1:size(horizontalLines, 1)
            %Draw the line
            line([horizontalLines(i, 1), horizontalLines(i, 2)] / k, [horizontalLines(i, 3), horizontalLines(i, 4)] / k, 'Color', 'Red', 'LineWidth', lineWidth)
        end
    end
    if (size(verticalLines, 1) ~= 2 || size(horizontalLines, 1) ~= 2)
        disp("Cound't find required lines")
        Ir = I;
        notProcessed = 2;
        return
    end
    %����� ����������� ���� �������������� � ������������ �����
    intersection_points = zeros(size(lines, 1), 2);
    for vi = 1:size(verticalLines, 1)
        vl = [verticalLines(vi, 1), verticalLines(vi, 3); verticalLines(vi, 2), verticalLines(vi, 4)];
        for hi = 1:size(horizontalLines, 1)
            hl = [horizontalLines(hi, 1), horizontalLines(hi, 3); horizontalLines(hi, 2), horizontalLines(hi, 4)];
            [x, y] = lines_intersection(vl,hl);
            intersection_points((vi - 1) * size(verticalLines, 1) + hi, 1) = x;
            intersection_points((vi - 1) * size(verticalLines, 1) + hi, 2) = y;
        end
    end
    mass_center = mean(intersection_points, 1);
    %��������� ��-�� ������ ����. � ���������� ����� ����� � ��. �������: �����
    %������� - 1, ������ ������� - 2, ����� ������ - 3, ������ ������ - 4
    [~, point_idx] = sortrows((intersection_points - mass_center) > 0, [2 1]);
    intersection_points = intersection_points(point_idx, :);
    %����� ����������� ���������� ��� ������������ ������������ �������
    %�����������
    diag_lt_rb = [intersection_points(1, 1) intersection_points(1, 2); intersection_points(4, 1) intersection_points(4, 2)];
    diag_lb_rt = [intersection_points(3, 1) intersection_points(3, 2); intersection_points(2, 1) intersection_points(2, 2)];
    [diag_intersection(1), diag_intersection(2)] = lines_intersection(diag_lt_rb, diag_lb_rt);
    %������� �������� ������
    top_width = pdist([intersection_points(1, :); intersection_points(2, :)]);
    bottom_width = pdist([intersection_points(3, :); intersection_points(4, :)]);
    original_width = (top_width + bottom_width) / 2 + 2 * abs(mass_center(1) - diag_intersection(1));
    %������� �������� ������
    left_height = pdist([intersection_points(1, :); intersection_points(3, :)]);
    right_height = pdist([intersection_points(2, :); intersection_points(4, :)]);
    original_height = (left_height + right_height) / 2 + 2 * abs(mass_center(2) - diag_intersection(2));
    %������� �������������� ��������
    original_points = [1 1; original_width 1; 1 original_height; original_width original_height];
    transform = estimateGeometricTransform(intersection_points / k, original_points / k, 'projective');
    outputView = imref2d(round([original_height / k; original_width / k]));
    Ir = imwarp(I, transform, 'OutputView',outputView);
    notProcessed = false;
    %���������� ����������: ��������, ����� � �.�.
    if debug
        lineWidth = 5;
        %diagonal
        line([diag_lt_rb(1, 1) diag_lt_rb(2, 1)] / k, [diag_lt_rb(1, 2) diag_lt_rb(2, 2)] / k, 'Color', 'Magenta', 'LineWidth', lineWidth)
        line([diag_lb_rt(1, 1) diag_lb_rt(2, 1)] / k, [diag_lb_rt(1, 2) diag_lb_rt(2, 2)] / k, 'Color', 'Magenta', 'LineWidth', lineWidth)
        hold on
        plot(intersection_points(:, 1) / k, intersection_points(:, 2) / k, 'g*')
        text(intersection_points(:, 1) / k, intersection_points(:, 2) / k, string(1:size(intersection_points, 1)), 'Color', 'White')
        plot(mass_center(1) / k, mass_center(2) / k, 'go')
        text(mass_center(1) / k, mass_center(2) / k, 'M', 'Color', 'White')
        plot(diag_intersection(1) / k, diag_intersection(2) / k, 'mo');
        text(diag_intersection(1) / k, diag_intersection(2) / k, 'D', 'Color', 'White')
        hold off
        %Display result
        subplot(1, 2, 2); imshow(Ir);
    end
end

