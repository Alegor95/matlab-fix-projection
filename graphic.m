clear all;
close all;
load common.mat targetDir recognizedSet
load([targetDir recognizedSet], 'ocrset');

z_degs = extractfield(ocrset, 'z_deg');
y_degs = extractfield(ocrset, 'y_deg');
x_degs = extractfield(ocrset, 'x_deg');
procents = 1 - extractfield(ocrset, 'procent');

scatter3(x_degs, y_degs, z_degs, 40, procents, 'filled');
cb = colorbar;
xlabel('X Degree')
ylabel('Y Degree')
zlabel('Z Degree')
cb.Label.String = 'Recongnised, %';