clear all;
directory = 'texts\';
targetDir = 'target\';
cuttingDir = 'cutting';
utilsDir = 'util';
distortionUtilsDir = 'distortion_utils';
distortionFileSet = 'fileset.mat';
cuttedFileSet = 'cuttedset.mat';
recognizedSet = 'recognized.mat';
originalTextImage = 'original_white.png';
originalText = {
'1 ��������'
'�� ������ ������ ������� ����������� ���������� �������� ������ ������������� �������� IT. ���� ������ ������� ����������� ���������� ���� ����������� ��� � 1962 ���� ������ �������� ��������, ������ � ��������� ��������� ��� �� ����� ��������� ������� ����������� ����������, ��������� ������� ������. ��� ������� � ����������� ��������� ������������ �����������, ������� ��������� � �������� ������� ������������ ���������������� �������, � ����� � ���������� �������� � ���������� ��������� �������� �� ���������� �������� � ������������.'
'������� ��������������� ������ ����������� ���������� ��������� ������������ �� ��� ��� �������� ���������, ��� � ��� �����������.'
'�������� ������������� ������ ����������� ���������� ��� �������� ����� ������� �������, � ������� ������������� ��������� ������������ ������� ������ ��� ������� �������������:'
'�������������� ��� �������'
'���������� ��� ��������'
'������� ��� �������� ��������'
'������� ����������'
'����� �������, ������� ����������� ���������� ����� ������� ����������, � ������ �������, � ��������� ��������.'
'� ������ ������� �������� ����� ��� ����� ����� ������� ������������ �����������, ����������� ������� ������������� ����������� ����������, ������������ ������� ����������� ���������� � ������� ���������� ���������� ��� ������ ����������, � ����� ����������� ����������� ��������.'
};
originalText = strjoin(originalText, newline);
save common.mat directory originalTextImage originalText...
    targetDir utilsDir distortionUtilsDir distortionFileSet...
    cuttingDir cuttedFileSet...
    recognizedSet