function [rotation_matrix] = get_zrotation(z_deg)
rotation_matrix = [
    cosd(z_deg) sind(z_deg) 0;...
    -sind(z_deg) cosd(z_deg)  0;...
    0           0            1
];
end

