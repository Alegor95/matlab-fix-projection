function [rotation_matrix] = get_xrotation(x_deg)
rotation_matrix = [
    1 0             0           ;...
    0 cosd(x_deg)   sind(x_deg);...
    0 -sind(x_deg)  cosd(x_deg)
];
end

