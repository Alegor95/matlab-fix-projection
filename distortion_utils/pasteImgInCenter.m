function [result] = pasteImgInCenter(target,input)
%PASTEIMGINCENTER Insert one input image in center of target
    [iw, ih, ~] = size(input);
    [tw, th, ~] = size(target);
    if iw > tw || ih > th
        ME = MException('pasteImgInCenter:inputTooBig', ...
        ['One of dimensions of input image (%d, %d)'...
        ' is bigger then dimension of tartget image (%d, %d)'],...
        iw, ih, tw, th);
        throw(ME);
    end
    shift_w = round((tw - iw) / 2);
    shift_h = round((th - ih) / 2);
    target_region = target(shift_w:shift_w+iw-1, shift_h:shift_h+ih-1, :);
    ind = find(input > 0);
    target_region(ind) = input(ind);
    result = target;
    result(shift_w:shift_w+iw-1, shift_h:shift_h+ih-1, :) = target_region;
end

