clear all;
close all;
load common.mat directory originalTextImage targetDir utilsDir cuttingDir
addpath(utilsDir); addpath(cuttingDir);
org = imread([directory originalTextImage]);
Ir = cutting(org, 'Debug', true);
if ~exist(targetDir, 'dir')
    mkdir(targetDir)
end
imwrite(Ir, [targetDir 'result.png']);