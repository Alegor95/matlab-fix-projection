% EXAMPLE
clear all
close all
load 'common.mat' targetDir directory originalTextImage originalText
%Original
origImg = imread([directory originalTextImage]);
originalRecognitionResult = ocr(origImg, 'Language', 'Russian');
distance = EditDistance(originalText, originalRecognitionResult.Text);
procent = distance / length(originalText);
sprintf('Original:\ndistance: %d, proc: %f', distance, procent)
%Recognized
textImg = imread([targetDir 'result.png']);
recognitionResult = ocr(textImg, 'Language', 'Russian');
distance = EditDistance(originalText, recognitionResult.Text);
procent = distance / length(originalText);
sprintf('Processed:\ndistance: %d, proc: %f', distance, procent)
%Display comparasion
wordCount = length(originalRecognitionResult.Words);
markedImage = origImg;
for i=1:wordCount
    markedImage = insertObjectAnnotation(markedImage, 'rectangle', originalRecognitionResult.WordBoundingBoxes(i, :), originalRecognitionResult.Words(i));
end
figure;
subplot(1, 2, 1); imshow(markedImage);
wordCount = length(recognitionResult.Words);
markedImage = textImg;
for i=1:wordCount
    markedImage = insertObjectAnnotation(markedImage, 'rectangle', recognitionResult.WordBoundingBoxes(i, :), recognitionResult.Words(i));
end
subplot(1, 2, 2); imshow(markedImage);
