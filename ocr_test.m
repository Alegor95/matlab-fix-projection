clear all;
close all;
load common.mat directory targetDir recognizedSet...
    cuttingDir cuttedFileSet utilsDir originalText
load([targetDir cuttedFileSet], 'cuttedset');
fileCount = length(cuttedset);
fprintf('Image to processing: %d\n', fileCount);
wb = waitbar(0, 'Cutting images...');
ocrset(fileCount) = struct;
for i=1:fileCount
    item = cuttedset(i);
    ocrset(i).x_deg = item.x_deg;
    ocrset(i).y_deg = item.y_deg;
    ocrset(i).z_deg = item.z_deg;
    ocrset(i).file = item.file;
    if item.wasNotProcessed ~= 0
        ocrset(i).distance = 0;
        ocrset(i).procent = 0;
        continue;
    end
    cutted = imread(item.file);
    recognitionResult = ocr(cutted, 'Language', 'Russian');
    dist = EditDistance(originalText, recognitionResult.Text);
    ocrset(i).distance = dist;
    ocrset(i).procent = dist / length(originalText);
    waitbar(i / fileCount, wb);
end
close(wb);
save([targetDir recognizedSet], 'ocrset');